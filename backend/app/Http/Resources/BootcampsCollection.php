<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Resources\BootcampCollection;


class BootcampsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
   //retornar la conexion o un arreglo con todos los bootcamps
   return ["sucess" => true ,
   "data" => $this->collection];
   
    }
}
